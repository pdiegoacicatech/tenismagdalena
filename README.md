# Bienvenido a la aplicación genérica para WordPress!

Con está aplicación podrás desarrollar un app para cualquier WordPress sin problemas. De aquí en adelante te explico los problemas con los que te puedes encontrar y cómo solucionarlos. 

## ¿Problemas con el #¢@∞ CORS?

Instala en el wordpress del que estés cogiendo el feed el siguiente plugin: https://es.wordpress.org/plugins/http-headers/ y dentro de settings > access control edita: Access-Control-Allow-Origin y añade dos urls: http://localhost:8080 para que funcione en iOs y http://localhost:8100 para que te funcione en local. 

## ¿Como poner subir a las appStores la aplicación?

Tanto para Android como para iOs lo primero que tienes que hacer es modificar la información de la app. **Todos los recursos necesarios están en pass y en la carpeta resources**
 - **config.xml**  tienes que modificar en widget id con el identificador de tu aplicación, y también tienes que cambiar tanto name como description. 
 - **iconos** para cambiar los iconos tienes que modificar el archivo icon y splash en la carpeta resources, cuidado con la resolución, tiene que ser bastante grande porque si no al ejecutar el comando: ```ionic cordova resources /platform/``` (el cual puedes ejecutar con la opción ```--icon``` o ```--splash```, te mostrará un error diciéndote que no puede generar algún icono. 
 - **src/app/constants.ts** modifica la variable current_app para que utilice el vector de configuraciones que necesites. 
 - **Configuración firebase**: Modificar en la home del proyecto los ficheros GoogleService-Info.plist y google-services.json
 - **Cambiar el SENDER_ID** Cambiar el SENDER_ID tanto en el package.json como en el app/app.components.ts. Pedreña: 980684852951, Tenis: 851746414096
 - Después de esto ejecuta estos comandos, para que así se reinstalen los plugins. 
    - ```ionic cordova platform rm ios```
    - ```ionic cordova platform rm android```
    - ```ionic cordova platform add ios```
    - ```ionic cordova platform add android@7.1.0```

 - Añadir google-services.json a: 
    - /Users/lcollado/Proyectos/wordpressApp/platforms/android/app/src/debug/google-services.json
    - /Users/lcollado/Proyectos/wordpressApp/platforms/android/app/google-services.json

 - Cuando compilemos la aplicación de ios hay que añadir el fichero de GoogleService-Info.plist a la home del proyecto.

### Apple Store

Para subir la aplicación al appStore es bastante sencillo, ejecutando el comando ```ionic cordova build ios --device``` se generará un proyecto que tienes que abrir con Xcode en la carpeta /platforms/ios, una vez lo abras podrás ejecutarlo en el móvil si lo tienes enchufado en el ordenador. 

Hay que seleccionar en los dispositivos la orientación vertical para iPhone y todas las orientaciones para iPad

Cuando hayas comprobado que la aplicación está funcionando correctamente puedes pulsar en el menú Product > Archive y verás que se generá un archivo que es el que después tendrás que subir a la appStore. 

Una vez subido lo verás en itunes connect dentro del apartado de actividad.

#### ¿Cómo activar tu cuenta de tester en apple?

En iTunes Connect tienes que añadir la nueva cuenta dentro de usuarios, al email que hayas puesto le llegará un email en el que tendrá que aceptar convertirse en desarrollador.

Tras tener el usuario con la aceptación en el listado de usuarios tienes que ir a la pestaña de TestFlight de la aplicación que quieras probar. Una vez ahí sólo tendrás que añadir al usuario. 

Esta pestaña sólo estará activada si anteriormente has subido un archivo de la aplicación


### Android

Primero ejecutar el comando: 
```ionic cordova run android --prod --release```

Firmar el apk:
```apksigner sign --ks pedrena-key.jks --out platforms/android/app/build/outputs/apk/release/app-release-unsigned-signed.apk platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk```

```apksigner sign --ks tenis-key.jks --out platforms/android/app/build/outputs/apk/release/app-release-unsigned-signed.apk platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk```
## PUSH

### PEDREÑA
Utilizar este tutorial: https://www.uno-de-piera.com/notificaciones-push-ionic-3-phonegap-push/
```ionic cordova plugin add phonegap-plugin-push --variable SENDER_ID=980684852951```
