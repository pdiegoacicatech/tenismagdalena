import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the LinkToWebviewPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'linkToWebview'
})
export class LinkToWebviewPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    var regex = /href="([\S]+).pdf"/g;
    var newString = value.replace(regex, "onClick=\"window.open('https://docs.google.com/gview?embedded=true&url=$1.pdf', '_blank', 'location=yes')\"");
    
    value = newString;

    value = newString;
    
    regex = /href="([\S]+)"/g;
    newString = value.replace(regex, "onClick=\"window.open('$1', '_blank', 'location=yes')\"");
    return newString;
  }
}
