import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the DateToStringPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'dateToString',
})
export class DateToStringPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, type: string) {
    var date = new Date(value);
    switch(type){
      case 'date':
        return new Date(date);
      default: 
        return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear();
    }
  }
}
