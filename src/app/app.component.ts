import { Component, ViewChild } from '@angular/core';
import { Config, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushOptions, PushObject } from '@ionic-native/push';
import { Device } from '@ionic-native/device';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { NewsPage } from '../pages/news/news';
import { VideosPage } from '../pages/videos/videos';
import { EventosPage } from '../pages/eventos/eventos';
import { LegalNewsPage } from '../pages/legal-news/legal-news';
import { FavoritesPage } from '../pages/favorites/favorites';
import { SettingsPage } from '../pages/settings/settings';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import * as constants from './constants';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, url: string}>;
  secondary_pages: Array<{title: string, component: any}>;

  logo: string;
  app: string;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private config: Config,
    private push: Push,
    private device: Device,
    public storage: Storage, 
    public apiServiceProvider: ApiServiceProvider ) {
    this.initializeApp();
    this.initializePush();

    this.logo = constants[constants.current_app].logoMenu;
    this.app = constants.current_app;
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage, url: null },
      { title: 'Noticias', component: NewsPage, url: null },
    ];


    if(constants[constants.current_app].info.has_legal && constants[constants.current_app].info.has_legal_link === '') {
      this.pages.push({ title: 'Acuerdos del Club', component: LegalNewsPage, url: null });
    }
    if(constants[constants.current_app].info.has_legal && constants[constants.current_app].info.has_legal_link !== '') {
      this.pages.push({ title: 'Acuerdos del Club', component: null, url: constants[constants.current_app].info.has_legal_link });
    }
    if(constants[constants.current_app].info.has_restaurant) {
      this.pages.push({ title: 'Restaurante', component: LegalNewsPage, url: null });
    }
    this.pages.push({ title: 'Eventos', component: EventosPage, url: null });
    if(constants[constants.current_app].info.has_videos) {
      this.pages.push({ title: 'Vídeos', component: VideosPage, url: null });
    }

    this.secondary_pages = [
      { title: 'Favoritos', component: FavoritesPage },
      { title: 'Configuración', component: SettingsPage }
    ];

    this.config.set('ios', 'backButtonText', 'Volver');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  
  initializePush() {
    this.platform.ready().then(() => {
      //alert('initializePush');
      if (!this.platform.is('cordova')) {
        console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
        return;
      }
      
      // comprobamos los permisos
      this.push.hasPermission()
        .then((res: any) => {
          if (res.isEnabled) {
            console.log('We have permission to send push notifications');
          } else {
            console.log('We do not have permission to send push notifications');
          }
        });
        
    
      // inicializamos la configuración para android y ios
      const options: PushOptions = {
        android: {
          senderID: '980684852951',
          clearBadge: true,
        },
        ios: {
          // senderID: '980684852951',
          alert: 'true',
          badge: true,
          sound: 'false',
          clearBadge: true,
        },
        windows: {}
      };
      
      const pushObject: PushObject = this.push.init(options);
      
      pushObject.on('notification').subscribe((data: any) => {
        pushObject.setApplicationIconBadgeNumber(0);
      });
      
      pushObject.getApplicationIconBadgeNumber().then((n) => {
        pushObject.setApplicationIconBadgeNumber(0);
      })

      const apiServiceProvider = this.apiServiceProvider;
      pushObject.on('registration').subscribe((registration: any) => {
          const registrationId = registration.registrationId;
          // alert(registrationId);
          apiServiceProvider.sendPushId(registrationId, this.platform.is('android') ? 'android' : 'ios', this.device.model, this.device.uuid);
          // registrationId lo debes guardar en mysql o similar para reutilizar
      });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component) {
      this.nav.setRoot(page.component);
    } else {
      window.open(page.url, '_blank', 'location=yes');
    }
  }
}
