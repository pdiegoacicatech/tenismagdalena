import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CalendarModule } from "ion2-calendar";
import { IonicStorageModule } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Push } from '@ionic-native/push';
// import { FCM } from '@ionic-native/fcm';
import { Device } from '@ionic-native/device';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { VideoPage } from '../pages/home/video';
import { NewsPage } from '../pages/news/news';
import { NewsViewPage } from '../pages/news-view/news-view';
import { VideosPage } from '../pages/videos/videos';
import { InfoPage } from '../pages/info/info';
import { EventosPage } from '../pages/eventos/eventos';
import { LegalNewsPage } from '../pages/legal-news/legal-news';
import { EventViewPage } from '../pages/events-view/events-view';
import { FavoritesPage } from '../pages/favorites/favorites';
import { SettingsPage } from '../pages/settings/settings';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { LinkToWebviewPipe } from '../pipes/link-to-webview/link-to-webview';
import { SafePipe } from '../pipes/safe/safe';
import { DateToStringPipe } from '../pipes/date-to-string/date-to-string';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    VideoPage,
    NewsPage,
    NewsViewPage,
    VideosPage,
    InfoPage,
    EventosPage,
    LegalNewsPage,
    EventViewPage,
    FavoritesPage,
    SettingsPage,
    LinkToWebviewPipe,
    SafePipe,
    DateToStringPipe,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    CalendarModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    VideoPage,
    NewsPage,
    NewsViewPage,
    VideosPage,
    InfoPage,
    EventosPage,
    LegalNewsPage,
    EventViewPage,
    FavoritesPage,
    SettingsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider,
    SocialSharing,
    Push,
    // FCM,
    Device,
  ]
})
export class AppModule {}
