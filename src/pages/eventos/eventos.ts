import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CalendarComponentOptions } from "ion2-calendar";
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { EventViewPage } from '../../pages/events-view/events-view';
import { Storage } from '@ionic/storage';
import * as constants from '../../app/constants';

/**
 * Generated class for the EventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html',
})
export class EventosPage {
  current_app: String;
  selectedDay: Date;
  events: any;
  options: CalendarComponentOptions = {
    weekStart: 1,
    from: new Date(2000, 0, 1),
    pickMode: 'single',
    weekdays: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    monthPickerFormat: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    showMonthPicker: false
  };
  isDataAvailable: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiServiceProvider: ApiServiceProvider, public storage: Storage) {
    this.isDataAvailable = false;
    this.current_app = constants.current_app;
    this.selectedDay = new Date();
    this.storage.get('events').then((val) => {
      if(val !== null) {
        this.events = val;
        this.options.daysConfig = [];
        for(let k of Object.keys(val)) {
          var d = val[k];
          if(d.startDate == d.endDate) {
            this.options.daysConfig.push({
              date: d.startDate,
              marked: true,
              subTitle: '◉',
            });
          } else {
            var endDate = new Date(d.endDate);
            var startDate = new Date(d.startDate);

            for (var day = startDate; day <= endDate; day.setDate(day.getDate() + 1)) {
              var day_str:any = day.getFullYear()+'-'+(day.getMonth()+1<10 ? '0': '')+(day.getMonth()+1)+'-'+(day.getDate()<10 ? '0': '')+day.getDate();
              this.options.daysConfig.push({
                date: day_str,
                marked: true,
                subTitle: '◉',
              });
            }
          }
        }
        this.isDataAvailable = true;
      }
    });
    this.getEvents();
  }

  getEvents() {
    this.apiServiceProvider.getEvents()
    .then(data => {
      this.storage.set('events', data);
      if(this.events) console.log('Eventos en cache '+Object.keys(this.events).length);

      if(this.events == null || Object.keys(this.events).length < Object.keys(data).length){
        this.options.daysConfig = [];
        for(let k of Object.keys(data)) {
          var d = data[k];
          if(d.startDate == d.endDate) {
            this.options.daysConfig.push({
              date: d.startDate,
              marked: true,
              subTitle: '◉',
            });
          } else {
            var endDate = new Date(d.endDate);
            var startDate = new Date(d.startDate);

            for (var day = startDate; day <= endDate; day.setDate(day.getDate() + 1)) {
              var day_str:any = day.getFullYear()+'-'+(day.getMonth()+1<10 ? '0': '')+(day.getMonth()+1)+'-'+(day.getDate()<10 ? '0': '')+day.getDate();
              this.options.daysConfig.push({
                date: day_str,
                marked: true,
                subTitle: '◉',
              });
            }
          }
        }
        this.isDataAvailable = true;
        this.events = data;
      } else {
        console.log('Ya están cargados los eventos');
      }
    });
  }

  onChange(e) {
    this.selectedDay = new Date(e);
  }

  navigate(event) {
    this.navCtrl.push(EventViewPage, {
      event,
    });
  }

  isBetween(selectedDate, e){
    selectedDate = new Date(selectedDate).setHours(0,0,0,0);
    var startDate = new Date(e.startDate).setHours(0,0,0,0);
    var endDate = new Date(e.endDate).setHours(0,0,0,0);
    return selectedDate >= startDate && selectedDate <= endDate;
  }
}
