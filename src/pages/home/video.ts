import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular';

@Component({
    template: `
  <ion-header>
    <ion-toolbar>
      <ion-title>
        {{title}}
      </ion-title>
      <ion-buttons start>
        <button ion-button (click)="dismiss()">
          <span ion-text color="primary" showWhen="ios">Cancelar</span>
          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
        </button>
      </ion-buttons>
    </ion-toolbar>
  </ion-header>
  <ion-content>
    <h1 style="margin-bottom: 30px;">{{title}}</h1>
    <iframe
        width="600"
        height="250"
        allow="autoplay; encrypted-media" 
        allowfullscreen
        frameborder="0" style="border:0"
        [src]="url | safe: 'resourceUrl'">
    </iframe>
  </ion-content>
  `
  })
  export class VideoPage {
    url;
    title;
  
    constructor(
      public platform: Platform,
      public params: NavParams,
      public viewCtrl: ViewController
    ) {
      this.url   = 'https://www.youtube.com/embed/' + this.getId(this.params.get('videoUrl'));
      this.title = this.params.get('videoTitle');
    }
  
    dismiss() {
      this.viewCtrl.dismiss();
    }

    getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
    
        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

  }