import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { NewsViewPage } from '../news-view/news-view';
import { EventViewPage } from '../events-view/events-view';
import { InfoPage } from '../info/info';
import * as constants from '../../app/constants';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { VideoPage } from './video';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  news: any;
  app: string;
  legalNews: any;
  n_legalNews: number;
  restaurantNews: any;
  n_restaurantNews: number;
  videos: any;
  background: string;
  logo: string;
  info: any;
  events: any;
  favs: Array<string> = [];
  lastAccess: Date;

  constructor(public navCtrl: NavController, public apiServiceProvider: ApiServiceProvider, public storage: Storage, 
      private socialSharing: SocialSharing, public modalCtrl: ModalController) {
    this.getNews();
    this.app = constants.current_app;
    this.storage.get('lastAccess').then((val) => {
      if(val !== null) {
        this.lastAccess = new Date(val);
        this.lastAccess.setMinutes(this.lastAccess.getMinutes() - 1);
      } else {
        this.lastAccess = new Date();
      }
      
      const now = new Date();
      this.storage.set('lastAccess', now.toISOString());
    });

    this.storage.get('news').then((val) => {
      if(val !== null) this.news = val;
    });

    if(constants[constants.current_app].info.has_videos) {
      this.getVideos();
      this.storage.get('videos').then((val) => {
        if(val !== null) this.videos = val;
      });
    }

    if(constants[constants.current_app].info.has_legal && constants[constants.current_app].info.has_legal_link === '') {
      this.getLegalNews();
      this.storage.get('legalNews').then((val) => {
        if(val !== null) this.legalNews = val;
      });
    }

    if(constants[constants.current_app].info.has_restaurant) {
      this.getRestaurantNews();
      this.storage.get('restaurantNews').then((val) => {
        if(val !== null) this.restaurantNews = val;
      });
    }

    this.getEvents();
    this.storage.get('events').then((data) => {
      if(data !== null) {
        this.events = [];
        Object.keys(data).forEach( (key) => {
          const value = data [key];
          const end = new Date();
          end.setDate(end.getDate() + 60);
          const today = new Date();
          if(new Date(value.startDate) <= end && new Date(value.startDate) >= today){
            this.events.push(value);
          }
        });
      }
    });

    this.background = constants[constants.current_app].background;
    this.logo = constants[constants.current_app].logo;
    this.info = constants[constants.current_app].info;

    this.storage.get('favs').then((val) => {
      if (val !== null) { this.favs = val;
      } else { this.favs = []; }
    });
  }

  share(message, subject, file, url) {
    this.socialSharing.share(message, subject, file, url);
  }

  getNews() {
    this.apiServiceProvider.getNews()
    .then(data => {
      this.news = data;
      this.storage.set('news', data);
    });
  }

  getVideos() {
    this.apiServiceProvider.getVideos()
    .then(data => {
      this.videos = data;
      this.storage.set('videos', data);
    });
  }

  getEvents() {
    this.apiServiceProvider.getEvents()
    .then(data => {
      this.events = [];

      Object.keys(data).forEach( (key) => {
        const value = data [key];
        const end = new Date();
        end.setDate(end.getDate() + 60);
        const today = new Date();
        today.setDate(end.getDate() - 0.5);
        if(new Date(value.startDate) <= end && new Date(value.startDate) >= today){
          this.events.push(value);
        }
      });
      
      console.log(data);
      this.storage.set('events', data);
    });
  }

  getLegalNews() {
    this.apiServiceProvider.getLegalNews()
    .then(data => {
      this.legalNews = data;
      this.n_legalNews = this.legalNews.length;
      this.storage.set('legalNews', data);
    });
  }

  getRestaurantNews() {
    this.apiServiceProvider.getRestaurantNews()
    .then(data => {
      this.restaurantNews = data;
      this.n_restaurantNews = this.restaurantNews.length;
      this.storage.set('restaurantNews', data);
    });
  }

  navigateToNews(news) {
    this.navCtrl.push(NewsViewPage, {
      news,
    });
  }

  navigateToEvent(event) {
    this.navCtrl.push(EventViewPage, {
      event,
    });
  }

  navigateToInfo() {
    this.navCtrl.push(InfoPage);
  }

  openVideo(url, option, string) {
    window.open(url, option, string);
  }

  openVideoIframe(url, title) {
    let modal = this.modalCtrl.create(VideoPage, { videoUrl: url, videoTitle: title });
    modal.present();
  }

  saveAsFav(object) {
    var storage = this.favs;
    if(storage === null){ storage = []; }

    storage.push(object.id);
    this.storage.set('favs', storage);
    this.favs = storage;

    this.storage.get('favs_objects').then((val) => {
      var objects = val;
      if (val === null) { objects = []; }

      objects.push(object);
      this.storage.set('favs_objects', objects);
    });
  }

  removeAsFav(object) {
    var storage = this.favs;

    storage.splice(storage.indexOf(object.id), 1);
    this.storage.set('favs', storage);
    this.favs = storage;

    this.storage.get('favs_objects').then((val) => {
      var objects = val;

      objects.forEach( (value, key, index) => {
        if(value.id === object.id){
          objects.splice(index, 1);
        }
      });
      this.storage.set('favs_objects', objects);
    });
  }

  isNew (date) {
    var eventDate = new Date(date);
    // console.log(name, ' Comparo fecha de evento', eventDate, 'con lastAccess', this.lastAccess, eventDate > this.lastAccess ? 'es Nuevo' : 'no es nuevo');
    if (this.lastAccess !== null && eventDate > this.lastAccess) {
      return true;
    }
    return false;
  }
}
