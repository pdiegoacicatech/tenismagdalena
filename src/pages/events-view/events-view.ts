import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as constants from '../../app/constants';
import { Navbar } from 'ionic-angular';
import { ViewChild } from '@angular/core';

/**
 * Generated class for the EventViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-events-view',
  templateUrl: 'events-view.html',
})
export class EventViewPage {
  event: any;
  favs: Array<string> = [];
  current_app: string = constants.current_app;
  prev_event: any = null;
  next_event: any = null;
  @ViewChild(Navbar) navBar: Navbar;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, private socialSharing: SocialSharing) {
    this.event = navParams.data.event;

    this.storage.get('favs').then((val) => {
      if (val !== null) { this.favs = val;
      } else { this.favs = []; }
    });

    this.storage.get('events').then((val) => {
      for(let k of Object.keys(val)) {
        const i = parseInt(k);
        if (val[k].id == this.event.id) {
          if (i > 0) {
            this.prev_event = val[i - 1];
          }
          if (i < val.length - 1) {
            this.next_event = val[i + 1];
          }
        }
      };
    });
  }

  // Back button
  ionViewDidLoad() {
    this.navBar.backButtonClick = () => {
      this.navCtrl.popToRoot();
    }
  }

  saveAsFav(object) {
    var storage = this.favs;
    if(storage === null){ storage = []; }

    storage.push(object.id);
    this.storage.set('favs', storage);
    this.favs = storage;

    this.storage.get('favs_objects').then((val) => {
      var objects = val;
      if (val === null) { objects = []; }

      objects.push(object);
      this.storage.set('favs_objects', objects);
    });
  }

  removeAsFav(object) {
    var storage = this.favs;

    storage.splice(storage.indexOf(object.id), 1);
    this.storage.set('favs', storage);
    this.favs = storage;

    this.storage.get('favs_objects').then((val) => {
      var objects = val;

      objects.forEach( (value, key, index) => {
        if(value.id === object.id){
          objects.splice(index, 1);
        }
      });
      this.storage.set('favs_objects', objects);
    });
  }

  openLink(url){
    window.open(url, '_blank', 'location=yes');
  }

  share(message, subject, file, url) {
    this.socialSharing.share(message, subject, file, url);
  }

  goNextEvent() {
    if (this.next_event !== null) {
      this.navCtrl.push(EventViewPage, {
        event: this.next_event,
      }, { animate: true, direction: 'forward' });
    }
  }

  goPrevEvent() {
    if (this.prev_event !== null) {
      this.navCtrl.push(EventViewPage, {
        event: this.prev_event,
      }, { animate: true, direction: 'back' });
    }
  }

  swipe(event) {
    // Siguiente noticia
    if (event.direction === 2 && this.next_event !== null) {
      let next_event = this.next_event;
      this.navCtrl.push(EventViewPage, {
        event: next_event,
      }, { animate: true, direction: 'forward' });
    }

    // Anterior noticia
    if (event.direction === 4 && this.prev_event !== null) {
      let prev_event = this.prev_event;
      this.navCtrl.push(EventViewPage, {
        event: prev_event,
      }, { animate: true, direction: 'back' });
    }
  }
}
