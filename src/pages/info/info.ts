import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as constants from '../../app/constants';
/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  info: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.info = constants[constants.current_app].info;
  }

  openLink(url, option, string) {
    window.open(url, option, string);
  }

}
