import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { NewsViewPage } from '../news-view/news-view';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class NewsPage {
  news: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiServiceProvider: ApiServiceProvider, public storage: Storage) {
    // If we navigated to this page, we will have an item available as a nav param
    this.storage.get('news').then((val) => {
      if(val !== null) this.news = val;
    });
    this.getNews();
  }

  getNews() {
    this.apiServiceProvider.getNews()
    .then(data => {
      this.news = data;
      this.storage.set('news', data);
    });
  }

  navigate(news) {
    this.navCtrl.push(NewsViewPage, {
      news,
    });
  }
}
