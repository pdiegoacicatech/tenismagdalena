import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { NewsViewPage } from '../news-view/news-view';
import * as constants from '../../app/constants';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-legal-news',
  templateUrl: 'legal-news.html'
})
export class LegalNewsPage {
  legalN: any;
  title: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public apiServiceProvider: ApiServiceProvider, public storage: Storage) {
    // If we navigated to this page, we will have an item available as a nav param
    if(constants[constants.current_app].info.has_legal){
      this.storage.get('legalNews').then((val) => {
        if(val !== null) this.legalN = val;
      });
      this.getLegalNews();
      this.title = "Acuerdos del Club";
    } else {
      this.storage.get('restaurantNews').then((val) => {
        if(val !== null) this.legalN = val;
      });
      this.getRestaurantNews();
      this.title = "Restaurante";
    }
  }

  getLegalNews() {
    this.apiServiceProvider.getLegalNews()
    .then(data => {
      this.legalN = data;
      this.storage.set('legalNews', data);
    });
  }

  getRestaurantNews() {
    this.apiServiceProvider.getRestaurantNews()
    .then(data => {
      this.legalN = data;
      this.storage.set('restaurantNews', data);
    });
  }

  navigate(news) {
    this.navCtrl.push(NewsViewPage, {
      news,
    });
  }
}
