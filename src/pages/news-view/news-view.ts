import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Navbar } from 'ionic-angular';
import { ViewChild } from '@angular/core';



/**
 * Generated class for the NewsViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-news-view',
  templateUrl: 'news-view.html',
})
export class NewsViewPage {
  news: any;
  favs: Array<string> = [];
  news_list: any;
  news_next: any = null;
  news_previous: any = null;
  @ViewChild(Navbar) navBar: Navbar;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, private socialSharing: SocialSharing) {
    this.news = navParams.data.news;
    this.storage.get('favs').then((val) => {
      if (val !== null) { this.favs = val;
      } else { this.favs = []; }
    });

    
    // Get all news
    this.storage.get('news').then((val) => {
      
      if (val !== null) this.news_list = val;

      // Get next an previous items
      this.news_list.forEach((element, index) => {
        
        if ( element.id == this.news.id ) {

          if ( index < this.news_list.length - 1 ) {
            this.news_next = this.news_list[index + 1];
          }

          if ( index > 0 ) {
            this.news_previous = this.news_list[index - 1];
          }
          
        }

      });
      
    });

  }

  // Back button
  ionViewDidLoad() {
    this.navBar.backButtonClick = () => {
      this.navCtrl.popToRoot();
    }
  }

  saveAsFav(object) {

    var storage = this.favs;

    if (storage === null){ storage = []; }

    storage.push(object.id);
    this.storage.set('favs', storage);
    this.favs = storage;

    this.storage.get('favs_objects').then((val) => {

      var objects = val;
      if (val === null) { objects = []; }

      objects.push(object);
      this.storage.set('favs_objects', objects);

    });
  }

  share(message, subject, file, url) {
    this.socialSharing.share(message, subject, file, url);
  }

  removeAsFav(object) {
    var storage = this.favs;

    storage.splice(storage.indexOf(object.id), 1);
    this.storage.set('favs', storage);
    this.favs = storage;

    this.storage.get('favs_objects').then((val) => {
      var objects = val;

      objects.forEach( (value, key, index) => {
        if (value.id === object.id){
          objects.splice(index, 1);
        }
      });
      this.storage.set('favs_objects', objects);
    });
  }

  navigateToNews(news, direction) {
    this.navCtrl.push(NewsViewPage, {
      news,
    }, { animate: true, direction });
  }

  swipe(event) {
    // Siguiente noticia
    if (event.direction === 2 && this.news_next !== null) {
      let news_next = this.news_next;
      this.navCtrl.push(NewsViewPage, {
        news: news_next,
      }, { animate: true, direction: 'forward' });
    }

    // Anterior noticia
    if (event.direction === 4 && this.news_previous !== null) {
      let news_previous = this.news_previous;
      this.navCtrl.push(NewsViewPage, {
        news: news_previous,
      }, { animate: true, direction: 'back' });
    }
  }


}
