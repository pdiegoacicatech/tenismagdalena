import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as constants from '../../app/constants';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  background: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public alertCtrl:AlertController) {
    this.background = constants[constants.current_app].background;
  }

  clearStorage(){
    this.storage.set('favs', null);
    this.storage.set('favs_objects', null);

    let alert = this.alertCtrl.create({
      title: 'Datos eliminados',
      subTitle: 'Todos tus datos han sido eliminados, a partir de ahora tus favoritos habrán desaparecido.',
      buttons: ['OK']
    });
    alert.present();
  }
}
