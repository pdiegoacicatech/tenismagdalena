import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NewsViewPage } from '../news-view/news-view';
import { EventViewPage } from '../events-view/events-view';

/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {
  favs: any;

  constructor(public navCtrl: NavController, public storage: Storage) {
    this.storage.get('favs_objects').then((val) => {
      if (val !== null) { this.favs = val;
      } else { this.favs = []; }
    });
  }
  
  navigate(fav){
    switch(fav.type) {
      case 'news':
        var news = fav;
        news.date = new Date(fav.date);
        this.navCtrl.push(NewsViewPage, {
          news,
        });
      break;
      case 'event':
        var event = fav;
        event.startDate = new Date(fav.startDate);
        event.endDate = new Date(fav.endDate);
        event.date = new Date(fav.date);
        this.navCtrl.push(EventViewPage, {
          event,
        });
      break;
      case 'video':
        window.open(fav.link, '_blank', 'location=yes');
      break;
    }
  }
}
