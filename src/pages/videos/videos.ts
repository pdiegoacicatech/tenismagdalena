import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html'
})
export class VideosPage {
  videos: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiServiceProvider: ApiServiceProvider, public storage: Storage) {
    // If we navigated to this page, we will have an item available as a nav param
    this.storage.get('videos').then((val) => {
      if(val !== null) this.videos = val;
    });
    this.getvideos();
  }

  getvideos() {
    this.apiServiceProvider.getVideos()
    .then(data => {
      this.videos = data;
      this.storage.set('videos', data);
    });
  }

  openVideo(url, option, string) {
    window.open(url, option, string);
  }
}
