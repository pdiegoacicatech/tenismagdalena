import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import xml2js from 'xml2js';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import * as constants from '../../app/constants';
/*
  Generated class for the ApiServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiServiceProvider {
  apiUrl: string = constants[constants.current_app].feed;
  debug = true;

  constructor(public http: HttpClient, public plt: Platform) {
    // console.log('Hello ApiServiceProvider Provider');
    if (this.plt.is('cordova')) {
      this.debug = false;
    }
  }

  sendPushId(id, os, model, uuid) {
    let news_url = this.apiUrl+constants[constants.current_app].push_registration+'push='+id+'&os='+os+'&model='+model+'&uuid='+uuid;
    return new Promise(resolve => {
      // alert(news_url);
      this.http.get(news_url, {responseType:'text'})
      .subscribe((data) => {
      }, err => {
        console.log(err);
      });
    });
  }

  getNews() {
    let news_url = this.apiUrl+constants[constants.current_app].feed_news;
    return new Promise(resolve => {
      this.http.get(news_url, {responseType:'text'})
      .subscribe((data) => {
        this.parseNews(data)
         .then((data)=>
         {
           resolve(data);
         });
      }, err => {
        console.log(err);
      });
    });
  }

  getLegalNews() {
    let news_url = this.apiUrl+constants[constants.current_app].feed_legal;
    return new Promise(resolve => {
      this.http.get(news_url, {responseType:'text'})
      .subscribe((data) => {
        this.parseNews(data)
         .then((data)=>
         {
            resolve(data);
         });
      }, err => {
        console.log(err);
      });
    });
  }

  getRestaurantNews() {
    let news_url = this.apiUrl+constants[constants.current_app].feed_restaurant;
    console.log(news_url);
    return new Promise(resolve => {
      this.http.get(news_url, {responseType:'text'})
      .subscribe((data) => {
        this.parseNews(data)
         .then((data)=>
         {
            resolve(data);
         });
      }, err => {
        console.log(err);
      });
    });
  }

  parseNews(data)
  {
     return new Promise(resolve =>
     {
        var k,
          arr    = [],
          parser = new xml2js.Parser({ trim: true, explicitArray: true });

        parser.parseString(data, function (err, result) {
          var obj = result.rss.channel[0].item;
          for(k in obj)
          {
            var item = obj[k];
            var regex = /src="(.*?)"/;
            var imgs = regex.exec(item['content:encoded'][0]);
            var img = imgs !== null ? imgs[1] : item.urlImg ? item.urlImg[0] :'';
            arr.push({
                type    : 'news',
                id      : item.link[0],
                date    : (item.pubDate[0].replace('+0000', '')),
                img     : img,
                category: item.category[0],
                author  : item['dc:creator'][0],
                title   : item.title[0],
                content : item['content:encoded'][0].replace(/<img[^>"']*((("[^"]*")|('[^']*'))[^"'>]*)*>/g,""),
            });
          }
          resolve(arr);
        });
     });
  }

  getVideos() {
    let video_url = constants[constants.current_app].feed_video;
    console.log(video_url);
    return new Promise(resolve => {
      this.http.get(video_url, {responseType:'json'})
      .subscribe((data) => {
        const arr = [];
        for(let item of data['items']) {
          // https://youtu.be/
          arr.push({
            type      : 'video',
            id        : 'https://youtu.be/'+item.id.videoId,
            link      : 'https://youtu.be/'+item.id.videoId,
            description: item.snippet.description,
            title     : item.snippet.title,
            date      : item.snippet.publishedAt,
            img       : item.snippet.thumbnails.default.url,
            thumbnail : item.snippet.thumbnails.default.url,
          });
        }
        console.log(arr);
        resolve(arr);
      }, err => {
        console.log(err);
      });
    });
  }


  getEvents() {
    let events_url = this.apiUrl+constants[constants.current_app].feed_events;
    console.log(events_url);
    return new Promise(resolve => {
      this.http.get(events_url, {responseType:'text'})
      .subscribe((data) => {
        this.parseEvents(data)
         .then((data)=>
         {
            resolve(data);
         });
      }, err => {
        console.log(err);
      });
    });
  }

parseEvents(data)
  {
     return new Promise(resolve =>
     {
        var k;
        var arr:Array<{type: string, id: string, title: string, date: Date, img: string, startDate: Date, endDate: Date, startTime: string, endTime: string, description: string, address: string, horarios: string, clasificacion: string}> = [];
        var parser = new xml2js.Parser({ trim: true, explicitArray: true });

        parser.parseString(data, function (err, result) {
          var obj = result.rss.channel[0].item;
          for(k in obj)
          {
            var item = obj[k];
            var address = item.description[0].substring(item.description[0].indexOf('<span id="address">'), item.description[0].length);
            address = address.replace(/<br\/>/g, '').replace(/<\/span>/g, '').replace('<span id="address">','');

            arr.push({
                type      : 'event',
                id        : item.link[0],
                title     : item.title[0],
                date      : (item.pubDate[0].replace('+0000', '')),
                img       : item.urlImg ? item.urlImg[0] : '',
                startDate : (item.startDate[0]),
                startTime : item.startTime[0].substring(0, item.startTime[0].length - 3),
                endTime   : item.endTime[0].substring(0, item.endTime[0].length - 3),
                endDate   : (item.endDate[0]),
                description: item.description[0],
                horarios  : item.link1 ? item.link1[0]['$']['url'] : null,
                clasificacion  : item.link2 ? item.link2[0]['$']['url'] : null,
                address   : 'https://www.google.com/maps/embed/v1/place?key=AIzaSyAnJA7ipjslQNcGezQcz_G52Jd2_npjICY&q='+address,
            });
          }
          // Ordeno por fecha el array de eventos
          arr.sort(function(a,b){
            return new Date(a.startDate).getTime() - new Date(b.startDate).getTime();
          });

          resolve(arr);
        });
     });
  }
}
